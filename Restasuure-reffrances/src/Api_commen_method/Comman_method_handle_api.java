package Api_commen_method;

import static io.restassured.RestAssured.given;

public class Comman_method_handle_api { // Step01 : create the class name as Comman_method_handle_api

	public static int post_statusCode(String requestbody, String endpoint) { // step2: create the method as a
																				// post_statusCode

		int statuscode = given().header("Content-Type", "application/json").body(requestbody).when().post(endpoint)
				.then().extract().statusCode(); // step3: create the integar variable as statuscode as
												// requestbody using given,header,body,when,then

		return statuscode; // step4 : step4 : validate the return type

	}

	public static String post_responsebody(String requestbody, String endpoint) {
		String responsebody = given().header("Content-Type", "application/json").body(requestbody).when().post(endpoint)
				.then().extract().response().asString();
		return responsebody;

	}

	public static int put_statusCode(String requestbody, String endpoint) {
		int statuscode = given().header("Content-Type", "application/json").body(requestbody).when().post(endpoint)
				.then().extract().statusCode();
		return statuscode;

	}

	public static String put_responsebody(String requestbody, String endpoint) {
		String responsebody = given().header("Content-Type", "application/json").body(requestbody).when().post(endpoint)
				.then().extract().response().asString();
		return responsebody;

	}

	public static int patch_statusCode(String requestbody, String endpoint) {
		int statuscode = given().header("Content-Type", "application/json").body(requestbody).when().post(endpoint)
				.then().extract().statusCode();
		return statuscode;

	}

	public static String patch_responsebody(String requestbody, String endpoint) {
		String responsebody = given().header("Content-Type", "application/json").body(requestbody).when().post(endpoint)
				.then().extract().response().asString();
		return responsebody;

	}

	public static int get_statusCode(String endpoint) {
		int statuscode = given().header("Content-Type", "application/json").when().post(endpoint).then().extract()
				.statusCode();
		return statuscode;

	}

	public static String get_responsebody(String endpoint) {
		String responsebody = given().header("Content-Type", "application/json").when().post(endpoint).then().extract()
				.response().asString();
		return responsebody;

	}

}
