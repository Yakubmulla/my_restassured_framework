package Basic_operater;

public class Simple {

	public static void main(String[] args) {
		// Arithmetic Operators
		
		int a = 20; // Decare the dataType int , assianing the varible a is 20
		int b = 40;
		System.out.println(a + b);
		System.out.println(a - b);
		System.out.println(a * b);
		System.out.println(a / b);
		System.out.println(a % b);

		// Relational Operators
		System.out.println(a < b);
		System.out.println(a > b);
		System.out.println(a <= b);
		System.out.println(a >= b);
		System.out.println(a == b);
		System.out.println(a != b);

		// Logical oprator
		boolean x = true;
		boolean y = false;

		System.out.println(x && y); // false
		System.out.println(x || y); // True
		System.out.println(!x); // false
		System.out.println(!y); // True

		// Assigment Operator
		int xx;
		xx = a;
		System.out.println(xx);
		int yy = b;
		System.out.println(yy);
		xx = 100;

		xx = xx + 2;
		System.out.println(xx);

		// Increment operator
		xx++; // xx=xx+1;

		System.out.println(xx);

		// Decrement operators
		xx--; // xx=xx-1

		System.out.println(xx);
		xx -= 8; // xx=xx-8;

	}

}
