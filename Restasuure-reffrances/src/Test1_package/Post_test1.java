package Test1_package;

import java.io.File;


import java.io.IOException;
import java.time.LocalDateTime;


import org.testng.Assert;
import org.testng.annotations.Test;

import Api_commen_method.Comman_method_handle_api;
import New_repository.post_request_repository;
import Utility_Common_Method.handle_api_logs;
import Utility_Common_Method.handle_directory;
import endpoint.post_endpoint;
import io.restassured.path.json.JsonPath;

public class Post_test1 extends Comman_method_handle_api {
	@Test
	public static void executor() throws IOException {
File log_dir= handle_directory.Create_log_directory("Post_test1_log");
		String requestbody =post_request_repository.post_request();
		String endpoint = post_endpoint.post_endpoint();

		for (int i = 0; i < 5; i++) {

			int statusCode = post_statusCode(requestbody, endpoint);
			System.out.println(statusCode);
			if (statusCode == 201) {

				String responsebody = post_responsebody(requestbody, endpoint);
				System.out.println(responsebody);
				handle_api_logs.avidence_creator(log_dir, "post_test1", endpoint, requestbody, responsebody);
				Post_test1.validatur(requestbody, responsebody);
				break;

			} else {
				System.out.println("Expected status code not found hence retraying");
			}
		}
	}

	public static void validatur(String requestbody, String responsebody) { 
		JsonPath jsp_req = new JsonPath(requestbody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		JsonPath jsp_res = new JsonPath(responsebody);
		String res_name = jsp_res.getString("name");
		System.out.println("Name is :" + res_name);
		String res_id = jsp_res.getString("id");
		System.out.println("Id is :" + res_id);
		String res_job = jsp_res.getString("job");
		System.out.println("job is :" + res_job);
		String res_createdAt = jsp_res.getString("createdAt");
		System.out.println("createdAt is :" + res_createdAt);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);

	}
}
