import static io.restassured.RestAssured.given;


import org.testng.Assert;

import io.restassured.path.xml.XmlPath;

public class Soap_refrance02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		String BaseURI="https://www.dataaccess.com";
		
		//step2 Declare the request body
		String requestBody="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n"
				+ "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\r\n"
				+ "  <soap:Body>\r\n"
				+ "    <NumberToWords xmlns=\"http://www.dataaccess.com/webservicesserver/\">\r\n"
				+ "      <ubiNum>500</ubiNum>\r\n"
				+ "    </NumberToWords>\r\n"
				+ "  </soap:Body>\r\n"
				+ "</soap:Envelope>";
		//step3 Trigger the api and fetch the responsebody
		//RestAssured.baseURI=BaseURI;
		String responseBody=given().header("Content-Type", "application/xml; charset=utf-8").body(requestBody).when().post("https://www.dataaccess.com/webservicesserver/NumberConversion.wso")
		.then().extract().response().getBody().asString();
		
		//Step4 print the response body
		System.out.println(responseBody);
		
		//step5 extract the response body parameter
		XmlPath Xml_res=new XmlPath(responseBody);
		String res_tag=Xml_res.getString("NumberToWordsResult");
		System.out.println(res_tag);
		
		//step6 Validate the responsebody
		Assert.assertEquals(res_tag, "forty five ");
	}

}
