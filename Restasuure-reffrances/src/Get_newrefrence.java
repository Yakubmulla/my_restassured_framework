import io.restassured.RestAssured;

import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;

public class Get_newrefrence {

	public static void main(String[] args) {

		int expected_id[] = { 7, 8, 9, 10, 11, 12 };
		String expected_firstname[] = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
		// step1 - declare base url
		RestAssured.baseURI = "https://reqres.in/";
		// step2 - configure the requestbody parameter and trigger the api

		String responsebody = given().when().get("api/users?page=2").then().extract().response().asString();

		System.out.println("response body is :" + responsebody);

		JsonPath jsp_res = new JsonPath(responsebody);
		JSONObject array_res = new JSONObject(responsebody);
		JSONArray dataarray = array_res.getJSONArray("data");
		System.out.println(dataarray);
		int count = dataarray.length();
		System.out.println(count);
		for (int i = 0; i < count; i++) {
			int res_id = dataarray.getJSONObject(i).getInt("id");
			System.out.println(res_id);
			int exp_id = expected_id[i];

			String res_firstname = dataarray.getJSONObject(i).getString("first_name");
			String exp_firstname = expected_firstname[i];

			Assert.assertEquals(res_id, exp_id);
			Assert.assertEquals(res_firstname, exp_firstname);

		}

	}

}
