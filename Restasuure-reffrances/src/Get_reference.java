import io.restassured.RestAssured;

import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;

public class Get_reference {

	public static void main(String[] args) {
		int expected_id[] = { 7, 8, 9, 10, 11, 12 };
		String expected_firstname[] = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
		String expected_lastname[] = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };
		String expected_email[] = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
				"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
		// step01: Decare the URL
		RestAssured.baseURI = "https://reqres.in/";

		// step2 - configure the requestbody parameter and trigger the api

		String responseBody = given().when().get("api/users?page=2").then().extract().response().asString();
		System.out.println("response body is :" + responseBody);
		JsonPath jsp_res = new JsonPath(responseBody);
		JSONObject array_res = new JSONObject(responseBody);
		JSONArray dataarray = array_res.getJSONArray("data");
		System.out.println(dataarray);
		int count = dataarray.length();
		System.out.println(count);
		for (int i = 0; i < count; i++) {
			int res_id = dataarray.getJSONObject(i).getInt("id");
			String res_frist_name = dataarray.getJSONObject(i).getString("first_name");
			String res_last_name = dataarray.getJSONObject(i).getString("last_name");
			String res_email = dataarray.getJSONObject(i).getString("amail");
			System.out.println(res_id);
			int exp_id = expected_id[i];
			String exp_firstname = expected_firstname[i];
			String exp_lastname = expected_lastname[i];
			String exp_email = expected_email[i];
			Assert.assertEquals(res_id, exp_id);
			Assert.assertEquals(res_frist_name, exp_firstname);
			Assert.assertEquals(res_last_name, exp_lastname);
			Assert.assertEquals(res_email, exp_email);

		}

	}

}
