package RestAssured_Partices;

import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

public class RS_Post_partice {

	public static void main(String[] args) {
		// Decare the Base URI
		RestAssured.baseURI = "https://reqres.in/api/users";
		// Request and Response body Parametrer Tringe the Api
		String requestbody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"leader\"\r\n" + "}";
		String responsebody = given().header("Content-Type", "application/json").body(requestbody).when()
				.post("api/users").then().extract().response().asString();
		// Create the Oject of Json path to request and Responsebody
		JsonPath jsp_req = new JsonPath(requestbody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime Currentdate = LocalDateTime.now();
		String expectedate = Currentdate.toString().substring(0, 11);

		// Create the Object off Responsebody
		JsonPath jsp_res = new JsonPath(responsebody);
		String res_name = jsp_res.getString("name");
		System.out.println("name is :"+res_name);
		String res_job = jsp_res.getString("job");
		System.out.println("job is :"+ res_job);
		String res_id = jsp_res.getString("id");
		System.out.println("id is :"+ res_id);
		String res_createdAt = jsp_res.getString("createdAt");
		System.out.println(" createdAt   is :"+res_createdAt);

		// Validate the responsebody

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);

	}

}
