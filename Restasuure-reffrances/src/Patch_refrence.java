import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
public class Patch_refrence {

	public static void main(String[] args) {
		// step1 : decare base URL
		// step1 : decare Base URL

		RestAssured.baseURI = "https://reqres.in/";

		// Step2 : request Parameter and triger the Api
		String requestbody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"leader\"\r\n" + "}";
		String responsebody = given().header("Content-Type", "application/json").body(requestbody).when()
				.post("api/users").then().log().all().extract().response().asString();
		// System.out.println("responsebody is :"+responsebody);
		// step3: create an object of Json path to parse the requestbody And
		// Responsebody
		JsonPath jsp_req = new JsonPath(requestbody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);
		JsonPath jsp_res = new JsonPath(responsebody);
		String res_name = jsp_res.getString("name");
		System.out.println("Name is :" + res_name);
		String res_id = jsp_res.getString("id");
		System.out.println("Id is :" + res_id);
		String res_job = jsp_res.getString("job");
		System.out.println("job is :" + res_job);
		String res_createdAt = jsp_res.getString("createdAt");
		System.out.println("createdAt is :" + res_createdAt);

		// step4 : Validate the responseBody
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
	}

}
