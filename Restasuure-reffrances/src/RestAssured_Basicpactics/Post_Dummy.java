package RestAssured_Basicpactics;

import java.io.*;

import io.restassured.RestAssured;
import io.restassured.response.ResponseBodyData;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.given;

public class Post_Dummy {

	public static void main(String[] args) throws IOException {
		// Step1: Decare the Base URL
		RestAssured.baseURI = "https://dummy.restapiexample.com";
		// Step2= Configer Request paramert And Tring The Api
		String responsebody = ((ResponseBodyData) ((RequestSpecification) given()
				.headers("Content-Type", "application/json")
				.body("{\r\n" + "    \"status\": \"success\",\r\n" + "    \"data\": {\r\n" + "        \"id\": 4053\r\n"
						+ "    },\r\n" + "    \"message\": \"Successfully! Record has been added.\"\r\n" + "}")
				.when().post("api/v1/create").then()).response()).asString();
		System.out.println("responsebody is :" + responsebody);

	}

}
